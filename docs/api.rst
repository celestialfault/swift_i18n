API Reference
==============

.. autoclass:: swift_i18n.Translator
  :members:
  :special-members: __call__

.. autoclass:: swift_i18n.Locale
  :members:

.. autoclass:: swift_i18n.LocaleMapping
  :members:

.. autoclass:: swift_i18n.Humanize
  :members:

Utils
-----

.. automodule:: swift_i18n.util
  :members:
