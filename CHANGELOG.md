# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.6.0] - 2020-08-20
### Added

- Built-in [Red](https://github.com/Cog-Creators/Red-DiscordBot) integration

## [0.5.1] - 2020-01-16
### Fixed
- Deep copying a LazyStr no longer attempts to format it

## [0.5.0] - 2020-01-10
### Added
- Basic functionality tests
- Transformers may now be added to modify format arguments before translation

### Changed
- The `Translator#locales` property can now be set with a list of locales directly
- Attempting to set the list of locales on a `TranslatorGroup` instance now raises a RuntimeError instead of the previous no-op behaviour
- Moved the `swift_i18n.LocaleMapping.ext` class variable to `swift_i18n.Locale.ext`
- Attempting to retrieve a non-existent string from a strict translator now raises a KeyError subclass: `swift_i18n.errors.NoSuchString`

### Deprecated
- `TranslatorGroup.generic()` is deprecated; use `TranslatorGroup.root()` instead

## [0.4.1] - 2019-08-30
### Fixed
- Package failing to install

## [0.4.0] - 2019-08-30
### Added
- This changelog file
- `swift_i18n.meta` package; this currently only contains a `__version__` variable, which denotes the current library version

### Changed
- Reduced examples down to one script, and added a basic formatting example
- Debug logging is now much more verbose in regards to logging what's currently being done
- Logging regarding values in lists is now clearer as to which item is being referred to
- KeyError exceptions raised during formatting are now extracted from the raised VisitationError and re-raised on their own

### Fixed
- Locales no longer raise an error if a locale unknown to Babel is used, but instead fall back to the system locale
- Group translators are now correctly considered strict if the parent translator is also strict

## [0.3.1] - 2019-08-20
### Fixed
- Default translations return values
- `Translator.group()` not correctly passing base keys to the returned grouped translator

## [0.3.0] - 2019-08-20
## Added
- Babel formatting helper

## [0.2] - 2019-08-14
### Added
- Simple example usage scripts
- Logging when messages cannot be correctly parsed

## [0.1] - 2019-08-14
### Added
- Initial release

[Unreleased]: https://gitlab.com/odinair/swift_i18n/compare/v0.5.1...HEAD
[0.5.1]: https://gitlab.com/odinair/swift_i18n/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/odinair/swift_i18n/compare/v0.4.1...v0.5.0
[0.4.1]: https://gitlab.com/odinair/swift_i18n/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/odinair/swift_i18n/compare/v0.3.1...v0.4.0
[0.3.1]: https://gitlab.com/odinair/swift_i18n/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/odinair/swift_i18n/compare/v0.2...v0.3.0
[0.2]: https://gitlab.com/odinair/swift_i18n/compare/v0.1...v0.2
[0.1]: https://gitlab.com/odinair/swift_i18n/-/tags/v0.1
