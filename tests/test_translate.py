import pytest

from swift_i18n import Translator, Humanize
from datetime import datetime, timedelta


@pytest.fixture()
def translate() -> Translator:
    return Translator(__file__, locales=["en-US"])


def test_simple(translate):
    assert translate("hello") == "Hello"


def test_format(translate):
    assert translate("hello_name", name="world") == "Hello, world"


def test_locale(translate):
    translate.locales = ["es-ES"]
    assert translate("hello") == "Hola"


def test_fuzzy_locale(translate):
    translate.locales = ["es"]
    assert translate("hello") == "Hola"


def test_fallthrough(translate):
    translate.locales = ["es-ES", "en-US"]
    assert translate("hello_name", name="world") == "Hello, world"


def test_plural(translate):
    assert translate("plural", num=0) == "zero"
    assert translate("plural", num=1) == "one"
    assert translate("plural", num=5) == "5"


def test_humanize(translate):
    # These are all (for the most part) safe to assert, because we'll always
    # be testing against en-US.
    assert translate("format", value=Humanize(1000)) == "1,000"
    assert (
        translate("format", value=Humanize(datetime(year=2019, month=8, day=16)))
        == "Aug 16, 2019, 12:00:00 AM"
    )
    assert translate("format", value=Humanize(timedelta(seconds=30))) == "30 seconds"
    assert translate("format", value=Humanize(lambda locale: locale)) == "en_US"
